const application = require("./application");
const { show } = require("./pattern");
const messages = require("./messages");

const mockClient = (emit, say = () => {}) => ({
  say,
  on: (_, handler) => {
    emit(handler);
  },
  disconnect: () => {},
});

const mockPort = (note) => ({
  note,
  close: () => {},
});

const channel = "test-channel";
const drums = {
  kick: "24",
  snare: "26",
  lo_tom: "2B",
  hi_tom: "32",
  oh: "2A",
  ch: "2E",
  clap: "27",
  clave: "4B",
  agogo: "43",
  crash: "31",
};

describe("application", () => {
  it("should be able to add drums to a pattern and play them", (done) => {
    let calls = 0;
    let fn;
    const client = mockClient((handler) => {
      handler(channel, "", "!drums kick 12 127", false);
      handler(channel, "", "!drums snare 12 127", false);
    });
    const port = mockPort((_, drum) => {
      if (drum === drums.kick || drum === drums.snare) {
        calls = calls + 1;
      }
      if (calls === 2) {
        fn();
        expect(true).toBe(true);
        done();
      }
    });
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
  it("should be able to modify velocity of drums", (done) => {
    let fn;
    const client = mockClient((handler) => {
      handler(channel, "", "!drums kick 1 127", false);
      handler(channel, "", "!drums kick 1 80", false);
    }, jest.fn());
    const port = mockPort((_, drum, vel) => {
      fn();
      expect(client.say).toHaveBeenNthCalledWith(
        1,
        channel,
        messages.inserted({ drum: "kick", position: 1, velocity: 127 })
      );
      expect(client.say).toHaveBeenNthCalledWith(
        2,
        channel,
        messages.inserted({ drum: "kick", position: 1, velocity: 80 })
      );
      expect(drum).toBe(drums["kick"]);
      expect(vel).toBe(80);
      done();
    });
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
  it("should be able to remove drums", (done) => {
    let calls = 0;
    let fn;
    const client = mockClient((handler) => {
      handler(channel, "", "!drums kick 1 127", false);
      handler(channel, "", "!drums snare 1 127", false);
      handler(channel, "", "!drums snare 1 0", false);
      handler(channel, "", "!drums kick 8 127", false);
    }, jest.fn());
    const port = mockPort((_, drum) => {
      if (drum === drums.kick) {
        calls = calls + 1;
      }
      if (drum === drums.snare) {
        fn();
        expect(false).toBe(true);
        return done();
      }
      if (calls === 2) {
        fn();
        expect(client.say).toHaveBeenNthCalledWith(
          1,
          channel,
          messages.inserted({ drum: "kick", position: 1, velocity: 127 })
        );
        expect(client.say).toHaveBeenNthCalledWith(
          2,
          channel,
          messages.inserted({ drum: "snare", position: 1, velocity: 127 })
        );
        expect(client.say).toHaveBeenNthCalledWith(
          3,
          channel,
          messages.removed({ drum: "snare", position: 1, velocity: 127 })
        );
        done();
      }
    });
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
  it("should return errors if msg isnt valid", (done) => {
    let fn;
    const client = mockClient(
      (handler) => {
        handler(channel, "", "!drums 444 81 abd", false);
      },
      (_, msg) => {
        expect(msg).toBe(
          [
            messages.errors.not_number("abd"),
            messages.errors.out_of_bounds("position"),
            messages.errors.not_drum,
          ].join(messages.errors.seperator)
        );
        fn();
        return done();
      }
    );
    const port = mockPort(() => {});
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
  it("should try to help with commands if asked for", (done) => {
    let fn;
    const client = mockClient(
      (handler) => {
        handler(channel, "", "!help", false);
      },
      (_, msg) => {
        expect(msg).toBe(messages.help.index);
        fn();
        return done();
      }
    );
    const port = mockPort(() => {});
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
  it("should try to help with pattern-command if asked for", (done) => {
    let fn;
    const client = mockClient(
      (handler) => {
        handler(channel, "", "!help pattern", false);
      },
      (_, msg) => {
        expect(msg).toBe(messages.help.pattern);
        fn();
        return done();
      }
    );
    const port = mockPort(() => {});
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
  it("should try to help with drum command if asked for", (done) => {
    let fn;
    const client = mockClient(
      (handler) => {
        handler(channel, "", "!help drums", false);
      },
      (_, msg) => {
        expect(msg).toBe(messages.help.drums(drums));
        fn();
        return done();
      }
    );
    const port = mockPort(() => {});
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
  it("should display the current pattern in chat", (done) => {
    let calls = 0;
    let fn;
    const client = mockClient(
      (handler) => {
        handler(channel, "", "!drums kick 12 127", false);
        handler(channel, "", "!drums snare 8 127", false);
        handler(channel, "", "!pattern drums", false);
      },
      (_, msg) => {
        calls += 1;
        if (calls == 3) {
          expect(msg).toBe(
            show([
              [],
              [],
              [],
              [],
              [],
              [],
              [],
              [{ drum: "snare", velocity: 127 }],
              [],
              [],
              [],
              [{ drum: "kick", velocity: 127 }],
              [],
              [],
              [],
              [],
            ])
          );
          fn();
          return done();
        }
      }
    );
    const port = mockPort(() => {});
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
  it("should display a proper error message if pattern is used incorrectly", (done) => {
    let fn;
    const client = mockClient(
      (handler) => {
        handler(channel, "", "!pattern", false);
      },
      (_, msg) => {
        expect(msg).toBe(messages.errors.not_a_track);
        fn();
        return done();
      }
    );
    const port = mockPort(() => {});
    const app = application(client, port, drums);
    fn = app.stop;
    app.start();
  });
});
