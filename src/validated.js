const Valid = (x) => ({
  __valid: true,
  map: (fn) => Valid(fn(x)),
  ap: (f) => f.map(x),
  flatMap: (fn) => fn(x),
  fold: (_, fn) => fn(x),
});

const Invalid = (x) => ({
  __valid: false,
  map: () => Invalid(x),
  ap: (f) =>
    f.fold(
      (y) => Invalid(y.concat(x)),
      () => Invalid(x)
    ),
  flatMap: () => Invalid(x),
  fold: (fn) => fn(x),
});

module.exports = {
  Valid,
  Invalid,
};
