const messages = {
  help: {
    index: `available commands are: !pattern, !drums`,
    pattern: `enter !pattern <track> where track is one of: drums`,
    drums: (drums) =>
      `enter "!drums <drum> <position> <velocity>" where drum is one of: ${Object.keys(
        drums
      ).join(", ")}`,
  },
  inserted: (drum) =>
    `${drum.drum} inserted at ${drum.position} with ${drum.velocity}`,
  removed: (drum) => `${drum.drum} removed from ${drum.position}`,
  synth: `synth not yet implemented`,
  errors: {
    not_a_track: "please enter a value for <track>",
    seperator: " | ",
    missing_value: (forWhat) => `missing value ${forWhat}`,
    length: "input is too long",
    drum_invalid: "invalid value for drum",
    not_drum: "not a drum sound",
    not_number: (n) => `${n} is not a number`,
    out_of_bounds: (what) => `value for ${what} out of bounds`,
  },
};

module.exports = messages;
