const bot = require("./bot");
const { sequencer } = require("./sequencer");

const application = (client, port, drumMapping) => {
  const seq = sequencer(port, drumMapping);
  const stop = () => {
    seq.stop();
    client.disconnect();
    port.close();
  };
  return {
    start: () => {
      seq.play();
      bot.listen(client, seq, drumMapping);
    },
    stop,
  };
};

module.exports = application;
