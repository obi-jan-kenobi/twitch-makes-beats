const { init, add, remove } = require("./pattern");

const MS_PER_MINUTE = 1000 * 60;
const STEP = {
  ADDED: "ADDED",
  REMOVED: "REMOVED",
};

const sequencer = (port, drumMapping) => {
  let bpm = 90;
  let __pattern = init();
  let interval;
  return {
    play: () => {
      let pos = 0;
      interval = setInterval(() => {
        __pattern[pos].forEach(hit(port, drumMapping));
        pos = next(pos);
      }, MS_PER_MINUTE / bpm / 4);
    },
    stop: () => {
      clearInterval(interval);
    },
    step: (drum) => {
      if (drum.velocity === 0) {
        __pattern = remove(__pattern)(drum);
        return STEP.REMOVED;
      } else {
        __pattern = add(__pattern)(drum);
        return STEP.ADDED;
      }
    },
    pattern: () => {
      return __pattern;
    },
  };
};

const hit = (port, drumMapping) => (note) => {
  port.note(4, drumMapping[note.drum], note.velocity, 50);
};

const next = (position) => (position === 15 ? 0 : position + 1);

module.exports = {
  STEP,
  sequencer,
};
