const messages = require("./messages");

const commands = (client) => (channel) => {
  client.say(channel, messages.help.index);
};

const drums = (drumMapping, client) => (channel) => {
  client.say(channel, messages.help.drums(drumMapping));
};

const pattern = (client) => (channel) => {
  client.say(channel, messages.help.pattern);
};

module.exports = (drumMapping, client) => ({
  commands: commands(client),
  drums: drums(drumMapping, client),
  pattern: pattern(client),
});
