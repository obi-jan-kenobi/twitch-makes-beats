const { Valid, Invalid } = require("./validated");

const Left = (x) => ({
  map: () => Left(x),
  flatMap: () => Left(x),
  fold: (f) => f(x),
});

const Right = (x) => ({
  map: (fn) => Right(fn(x)),
  flatMap: (fn) => fn(x),
  fold: (_, f) => f(x),
});

const id = (x) => x;

const toValidated = (either, leftFn = id) =>
  either.fold(
    (l) => Invalid(leftFn(l)),
    (r) => Valid(r)
  );

module.exports = {
  Left,
  Right,
  toValidated,
};
