const { parseDrums } = require("./drums");
const { Left, Right, toValidated } = require("./either");
const { Valid, Invalid } = require("./validated");
const { STEP } = require("./sequencer");
const { show } = require("./pattern");
const help = require("./help");
const messages = require("./messages");

const commands = {
  HELP: { commands: "!help", drums: "!help drums", pattern: "!help pattern" },
  DRUMS: "!drums",
  PATTERN: "!pattern",
};

const messageHandler =
  (client, sequencer, drumMapping) => (channel, _, message, self) => {
    const h = help(drumMapping, client);
    const isShort = checkLength(32);
    if (self) return;
    const msg =
      message && typeof message === "string" ? message.toLowerCase() : "";
    if (msg === commands.HELP.commands) {
      return h.commands(channel);
    }
    if (msg === commands.HELP.drums) {
      return h.drums(channel);
    }
    if (msg === commands.HELP.pattern) {
      return h.pattern(channel);
    }
    if (msg.startsWith(commands.DRUMS)) {
      return toValidated(isShort(msg), (e) => [e]).map(() =>
        parseDrums(drumMapping)(msg).fold(
          (errors) =>
            client.say(channel, errors.join(messages.errors.seperator)),
          (drum) => {
            const result = sequencer.step(drum);
            if (result === STEP.ADDED) {
              return client.say(channel, messages.inserted(drum));
            } else {
              return client.say(channel, messages.removed(drum));
            }
          }
        )
      );
    }
    if (msg.startsWith(commands.PATTERN)) {
      return toValidated(isShort(msg))
        .flatMap((m) => {
          const [, track] = m.split(" ");
          if (track === "drums") {
            const pattern = sequencer.pattern();
            return Valid(show(pattern));
          }
          return Invalid([messages.errors.not_a_track]);
        })
        .fold(
          (errors) =>
            client.say(channel, errors.join(messages.errors.seperator)),
          (pattern) => client.say(channel, pattern)
        );
    }
    if (msg.startsWith("!synth")) {
      return client.say(channel, messages.synth);
    }
  };

const listen = (client, sequencer, drumMapping) =>
  client.on("message", messageHandler(client, sequencer, drumMapping));

const checkLength = (length) => (str) =>
  str.length > length ? Left(messages.errors.length) : Right(str);

module.exports = {
  listen,
};
