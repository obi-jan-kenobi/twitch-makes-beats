const init = () => [
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
  [],
];

const add = (pattern) => (drum) =>
  [
    ...pattern.slice(0, drum.position - 1),
    [...pattern[drum.position - 1].filter((d) => d.drum !== drum.drum), drum],
    ...pattern.slice(drum.position),
  ];

const remove = (pattern) => (drum) =>
  [
    ...pattern.slice(0, drum.position - 1),
    [...pattern[drum.position - 1].filter((d) => d.drum !== drum.drum)],
    ...pattern.slice(drum.position),
  ];

const show = (pattern) =>
  pattern
    .map(
      (p, i) =>
        `${i + 1}: ${p.map((n) => `${n.drum}(${n.velocity})`).join(" - ")}`
    )
    .join(" | ");

module.exports = {
  init,
  add,
  remove,
  show,
};
