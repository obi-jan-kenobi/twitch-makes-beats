const { Left, Right, toValidated } = require("./either");
const { Valid, Invalid } = require("./validated");
const messages = require("./messages");

const drum = (d) => (p) => (v) => ({
  drum: d,
  position: p,
  velocity: v,
});

const parseDrum = (drumMapping) => (d) =>
  toValidated(isDefined(messages.errors.missing_value("drum"))(d), (e) => [e])
    .flatMap((drum) =>
      drum && typeof drum === "string"
        ? Valid(d.toLowerCase())
        : Invalid([messages.errors.drum_invalid])
    )
    .flatMap((drum) =>
      drumMapping[drum] ? Valid(drum) : Invalid([messages.errors.not_drum])
    );

const isDefined = (msg) => (x) => x ? Right(x) : Left(msg);

const parseNumber = (n) => {
  const number = n && +n;
  return typeof number === "number" && !isNaN(number)
    ? Right(number)
    : Left(messages.errors.not_number(n));
};

const parsePosition = (p) =>
  toValidated(parseNumber(p), (e) => [e]).flatMap((pos) =>
    pos > 0 && pos < 17
      ? Valid(pos)
      : Invalid([messages.errors.out_of_bounds("position")])
  );

const parseVelocity = (v) =>
  toValidated(parseNumber(v), (e) => [e]).flatMap((vel) =>
    vel >= 0 && vel <= 127
      ? Valid(vel)
      : Invalid([messages.errors.out_of_bounds("velocity")])
  );

const parseDrums = (drumMapping) => (str) => {
  const [, d, p, v] = str.split(" ");
  return Valid(drum)
    .ap(parseDrum(drumMapping)(d))
    .ap(parsePosition(p))
    .ap(parseVelocity(v || "0"));
};

module.exports = { parseDrums };
