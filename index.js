const tmi = require("tmi.js");
const JZZ = require("jzz");

const application = require("./src/application");
const config = require("./config");

const client = new tmi.Client({
  options: { debug: config.debug },
  connection: {
    reconnect: true,
    secure: true,
  },
  identity: {
    username: config.username,
    password: config.password,
  },
  channels: [config.username],
});

const drumMapping = require(config.mapping.drums);

client.connect().then(() => {
  const port = JZZ()
    .or("Cannot start MIDI engine!")
    .openMidiOut(config.midi.device)
    .or("Cannot open MIDI Out port!");
  application(client, port, drumMapping).start();
});
