const config = {
  username: process.env.CHANNEL,
  password: process.env.PASSWORD,
  debug: process.env.NODE_ENV !== "production",
  midi: {
    device: process.env.MIDI_DEVICE,
  },
  mapping: {
    drums: process.env.DRUM_MAPPING_PATH,
  },
};

module.exports = config;
